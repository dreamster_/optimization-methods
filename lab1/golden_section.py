# Метод золотого сечения, вариант 30

import math

def function(x):
	return math.pow(math.cos(x), 5) * (x + 1 / x)

a, b = 21, 23
e = 0.5
lamb = 1.618

x1 = b - ((b - a) / lamb)
x2 = a + ((b - a) / lamb)
y1, y2 = function(x1), function(x2)

while (b - a <= 2 * lamb * e):
	if (y1 <= y2):
		b = x2
		x2 = x1
		y2 = y1
		x1 = a + b - x2
		y1 = function(x1)
	else:
		a = x1
		x1 = x2
		y1 = y2
		x2 = a + b - x1
		y2 = function(x2)

if (y1 < y2):
	b = x2
else:
	a = x1

x_result = (a + b) / 2
print("Result: ", x_result)