# Метод дихотомии

import math

def function(x):
	return math.pow(math.cos(x), 5) * (x + 1 / x)

a, b = 21, 23
e = 0.5
h = 0.005

while (b - a > 2 * e):
	x = (a + b) / 2
	y1, y2 = function(x - h), function(x + h)

	if (y1 <= y2):
		b = x + h
	else:
		a = x - h

x_result = (a + b) / 2
print("Result: ", x_result)
