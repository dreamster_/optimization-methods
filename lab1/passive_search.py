# Метод пассивного поиска, вариант 30

import math

def function(x):
	return math.pow(math.cos(x), 5) * (x + 1 / x)

a, b = 21, 23
h = 0.2
n = (b - a) / h + 1

x_min = a
f_x_min = function(x_min)

for i in range(1, int(n)):
	x = a + ((b - a) / (n + 1)) * i
	f_x = function(x)
	if (f_x < f_x_min):
		f_x_min = f_x
		x_min = x

print("Result: ", x_min)
