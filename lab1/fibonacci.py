# Метод Фибоначчи, вариант 30

import math

def function(x):
	return math.pow(math.cos(x), 5) * (x + 1 / x)

a, b = 21, 23
n = 11
d = 0.01
fib_list = [1, 1,]

for i in range(2, n):
	f = fib_list[i-1] + fib_list[i-2]
	fib_list.append(f)

x1 = a + (b - a) * fib_list[n-3] / fib_list[n-1]
x2 = a + (b - a) * fib_list[n-2] / fib_list[n-1]
y1 = function(x1)
y2 = function(x2)

for i in range(n-3):
	if (y1 <= y2):
		b = x2
		x2 = x1
		y2 = y1
		x1 = a + b - x2
		y1 = function(x1)
	else:
		a = x1
		x1 = x2
		y1 = y2
		x2 = a + b - x1
		y2 = function(x2)

if (y1 < y2):
	b = x2
	x2 = x1
	y2 = y1
else:
	a = x1

x1 = x2 - d
y1 = function(x1)

if (y1 < y2):
	b = x2
else:
	a = x1

x_result = (a + b) / 2
print("Result: ", x_result)